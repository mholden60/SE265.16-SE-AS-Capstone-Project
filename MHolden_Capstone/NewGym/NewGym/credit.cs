﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace NewGym
{
    public partial class credit : Form
    {
        public credit()
        {
            InitializeComponent();
        }

        private void credit_Load(object sender, EventArgs e)
        {

        }

        private void btnNewRegister_Click(object sender, EventArgs e)
        {
            this.Hide();
            new Form1().Show();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.Hide();
            new search().Show();
        }

        private void btnRequest_Click(object sender, EventArgs e)
        {
            this.Hide();
            new classR().Show();
        }

        private void btnSurvey_Click(object sender, EventArgs e)
        {
            this.Hide();
            new survey().Show();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            PersonV2 temp;
            temp = new PersonV2();
            temp.CName = txtCname.Text;
            temp.CNumber = txtCnumber.Text;
            temp.CVS = txtCvs.Text;
            temp.Expire = dtExp.Text;


            if (temp.Feedback.Contains("ERROR: "))
            {
                lblFeedback.Text = temp.Feedback;
            }
            else
            {
                lblFeedback.Text = temp.AddCard();
            }
        }

        private void txtPay_TextChanged(object sender, EventArgs e)
        {

            
        }

        private void txtCnumber_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void txtCvs_TextChanged(object sender, KeyPressEventArgs e)
        {
           
        }

        private void txtCnumber_TextChanged(object sender, EventArgs e)
        {

        }

        
    }
}
