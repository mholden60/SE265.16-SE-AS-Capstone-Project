﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Sql;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace NewGym
{
   public class Person
    {
        private string feedback;
        private string txtFname;
        private string txtLname;
        private string txtRequestFN;
        private string txtSearchFN;
        private string txtRequestLN;
        private string txtSearchLN;
        private string txtUname;
        private string txtPword;
        private string txtAddress;
        private string txtPhone;
        private string txtCvs;
        private string cbClass;
        private string cbInstructor;
        private string cbGender;
        private string dtExp;
        private string dtRequest;
        private string cbPrivate;
        private string dtDob;
        private string txtCname;
        private string txtCnumber;
        private string txtCity;
        private string txtZip;
        private string cbState;
        private string cbStay;
        private string cbNeeds;
        private string cbReturn;
        private string txtName;
        private string txtComment;
        private string txtPay;
        public string Feedback
        {
            get { return feedback; }
        }

        public Person()
        {
            feedback = "";
        }

        public string FName
        {
            get { return txtFname; }
            set 
            {
                if (ValidationLibrary.IsItFilledIn(value, 1) == false)
                    feedback += "ERROR: Invalid First Name...Please insert First Name. \n";
                else txtFname = value;
            }
        }
        public string LName
        {
            get { return txtLname; }
            set
            {
                if (ValidationLibrary.IsItFilledIn(value, 1) == false)
                    feedback += "ERROR: Invalid First Name...Please insert Last Name. \n";
                else txtLname = value;
            }
        }

        public string City
        {
            get { return txtCity; }
            set
            {
                if (ValidationLibrary.IsItFilledIn(value, 1) == false)
                    feedback += "ERROR: Invalid City...Please insert City. \n";
                else txtCity = value;
            }  
        }
        public string Zip
        {
            get { return txtZip; }
            set
            {
                if (ValidationLibrary.Zipcode(value, 1) == false)
                    feedback += "ERROR: Please Enter Zip. \n";
                else if (ValidationLibrary.Zipcode(value, 1) == false)
                    feedback += "ERROR: Invalid Zip Code...Please enter valid Zip Code. \n";
                else txtZip = value;
            }

        }
        public string RequestFN
        {
            get { return txtRequestFN; }
            set
            {
                if (ValidationLibrary.IsItFilledIn(value, 1) == false)
                    feedback += "ERROR: Invalid First Name...Please insert First Name. \n";
                else txtRequestFN = value;
            }
        }
        public string RequestLN
        {
            get { return txtRequestLN; }
            set
            {
                if (ValidationLibrary.IsItFilledIn(value, 1) == false)
                    feedback += "ERROR: Invalid First Name...Please insert First Name. \n";
                else txtRequestLN = value;
            }
        }
        public string SearchFN
        {
            get { return txtSearchFN; }
            set
            {
                if (ValidationLibrary.IsItFilledIn(value, 1) == false)
                    feedback += "ERROR: Invalid First Name...Please insert First Name. \n";
                else txtSearchFN = value;
            }
        }
        public string SearchLN
        {
            get { return txtSearchLN; }
            set
            {
                if (ValidationLibrary.IsItFilledIn(value, 1) == false)
                    feedback += "ERROR: Invalid First Name...Please insert First Name. \n";
                else txtSearchLN = value;
            }
        }
        public string Uname
        {
            get { return txtUname; }
            set 
            {
                if (ValidationLibrary.IsItFilledIn(value, 1) == false)
                    feedback += "ERROR: Invalid Username...Please insert Username. \n";
                else if (ValidationLibrary.IsValidEmail(value) == false)
                    feedback += "ERROR: Invalid Username...Please enter correct Username in proper format. \n";
                else txtUname = value;
            }
        }
        public string Pword
        {
            get { return txtPword; }
            set
            {
                if (ValidationLibrary.IsItFilledIn(value, 1) == false)
                    feedback += "ERROR: Invalid Password...Please insert Password. \n";
                else txtPword = value;
            }
        }
        public string Address
        {
            get { return txtAddress; }
            set 
            {
                if (ValidationLibrary.IsItFilledIn(value, 1) == false)
                    feedback += "ERROR: Invalid Address...Please insert Address. \n";
                else txtAddress = value;
            }
        }
        public string Birth
        {
            get { return dtDob; }
            set { dtDob = value; }
        }
        public string Payment
        {
            
            get { return txtPay; }
        set 
        {
            if (ValidationLibrary.IsItFilledIn(value, 1) == false)
            {
                feedback += "ERROR: Invalid Payment...Please insert amount paid. \n";
            }
            else if (Convert.ToInt32(txtPay) < 0)
            {
                feedback += "ERROR: Invalid Payment...Please insert minimum amount. \n";
            }
            else txtPay = value;
        }
        }
        public string Phone
        {
            get {return txtPhone; }
            set 
            {
                if (ValidationLibrary.IsItFilledIn(value, 1) == false)
                {
                    feedback += "ERROR: Invalid Phone...Please insert Phone Number. \n";
                }
                
                else if (ValidationLibrary.phone(value, 1) == false)
                {
                    feedback += "ERROR: Please enter valid Phone Number. \n";
                }
                else if (ValidationLibrary.IsValidNumber(value) == false)
                {
                    feedback += "ERROR: Please use Digits Only. \n";
                }
                else txtPhone = value;
            }
            }

        public string CVS
            {
            
            get {return txtCvs; }
            set 
            {
                if (ValidationLibrary.IsItFilledIn(value, 1) == false)
                {
                    feedback += "ERROR: Invalid CVS...Please insert CVS. \n";
                }
                else if (ValidationLibrary.IsValidNumber(value) == false)
                {
                    feedback += "ERROR: Please use Digits Only. \n";
                }
                else txtCvs = value;
            }
        }
        public string State
        {
            get { return cbState; }
            set
            {
                if (ValidationLibrary.chooseState(value) == false)
                    feedback += "ERROR: Please Choose a State.\n";
                else
                    cbState = value;
            }
        }
        public string Stay
        {
            get { return cbStay; }
            set{ cbStay = value;}
        }
        public string Needs
        {
            get { return cbNeeds; }
            set{ cbNeeds = value;}
        }
        public string willReturn
        {
            get { return cbReturn; }
            set{ cbReturn = value;}
        }
        public string Name
        {
            get { return txtName; }
            set{txtName = value;}
        }
        public string Comment
        {
            get { return txtComment; }
            set { txtComment = value; }
        }
        public string Class
        {
            get{return cbClass;}
            set{cbClass = value;}
        }
        public string Instructor
        {
            get{return cbInstructor;}
            set{cbInstructor = value;}
        }
        public string Gender
        {
            get { return cbGender; }
            set
            {
                if (ValidationLibrary.chooseState(value) == false)
                    feedback += "ERROR: Please Choose a Gender.\n";
                else
                    cbGender = value;
            }
        }
        public string Expire
        {
            get{return dtExp;}
            set{dtExp = value;}
        }
        public string dateRequest
        {
            get {return dtRequest;}
            set {dtRequest = value;}
        }
        
        public string CName
        {
            get { return txtCname; }
            set
            {
                if (ValidationLibrary.IsItFilledIn(value, 1) == false)
                {
                    feedback += "ERROR: Please fill in Credit Card Name. \n";
                }
                txtCname = value;
            
            }
        }
        public string CNumber
        {
            get { return txtCnumber; }
            set
            {
                if (ValidationLibrary.IsItFilledIn(value, 1) == false)
                {
                    feedback += "ERROR: Please enter Credit card number. \n";
                }
                else if (ValidationLibrary.IsValidNumber(value) == false)
                {
                    feedback += "ERROR: Please use Digits Only. \n";
                }
                txtCnumber = value;
            }}
        public string Private
        {
            get {return cbPrivate;}
            set { cbPrivate = value;}
        }
        
        }
        
    }
        
