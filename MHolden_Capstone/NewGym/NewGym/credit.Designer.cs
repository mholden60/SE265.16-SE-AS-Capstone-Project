﻿namespace NewGym
{
    partial class credit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCname = new System.Windows.Forms.TextBox();
            this.txtCnumber = new System.Windows.Forms.TextBox();
            this.txtCvs = new System.Windows.Forms.TextBox();
            this.dtExp = new System.Windows.Forms.DateTimePicker();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.lblFeedback = new System.Windows.Forms.Label();
            this.btnRequest = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnNewRegister = new System.Windows.Forms.Button();
            this.btnSurvey = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(43, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(296, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "Credit Card Information";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Card Holder Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Card Number";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 117);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(28, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "CVS";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 145);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Expiration Date";
            // 
            // txtCname
            // 
            this.txtCname.Location = new System.Drawing.Point(113, 51);
            this.txtCname.Name = "txtCname";
            this.txtCname.Size = new System.Drawing.Size(100, 20);
            this.txtCname.TabIndex = 5;
            // 
            // txtCnumber
            // 
            this.txtCnumber.Location = new System.Drawing.Point(113, 83);
            this.txtCnumber.MaxLength = 16;
            this.txtCnumber.Name = "txtCnumber";
            this.txtCnumber.Size = new System.Drawing.Size(100, 20);
            this.txtCnumber.TabIndex = 6;
            this.txtCnumber.TextChanged += new System.EventHandler(this.txtCnumber_TextChanged);
            // 
            // txtCvs
            // 
            this.txtCvs.Location = new System.Drawing.Point(113, 114);
            this.txtCvs.MaxLength = 3;
            this.txtCvs.Name = "txtCvs";
            this.txtCvs.Size = new System.Drawing.Size(100, 20);
            this.txtCvs.TabIndex = 7;
            // 
            // dtExp
            // 
            this.dtExp.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtExp.Location = new System.Drawing.Point(113, 145);
            this.dtExp.Name = "dtExp";
            this.dtExp.Size = new System.Drawing.Size(100, 20);
            this.dtExp.TabIndex = 8;
            // 
            // btnSubmit
            // 
            this.btnSubmit.Location = new System.Drawing.Point(113, 221);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(75, 23);
            this.btnSubmit.TabIndex = 9;
            this.btnSubmit.Text = "Submit";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // lblFeedback
            // 
            this.lblFeedback.AutoSize = true;
            this.lblFeedback.Location = new System.Drawing.Point(13, 257);
            this.lblFeedback.Name = "lblFeedback";
            this.lblFeedback.Size = new System.Drawing.Size(55, 13);
            this.lblFeedback.TabIndex = 10;
            this.lblFeedback.Text = "Feedback";
            // 
            // btnRequest
            // 
            this.btnRequest.Location = new System.Drawing.Point(264, 107);
            this.btnRequest.Name = "btnRequest";
            this.btnRequest.Size = new System.Drawing.Size(75, 23);
            this.btnRequest.TabIndex = 16;
            this.btnRequest.Text = "Class Request";
            this.btnRequest.UseVisualStyleBackColor = true;
            this.btnRequest.Click += new System.EventHandler(this.btnRequest_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(264, 78);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 15;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnNewRegister
            // 
            this.btnNewRegister.Location = new System.Drawing.Point(264, 49);
            this.btnNewRegister.Name = "btnNewRegister";
            this.btnNewRegister.Size = new System.Drawing.Size(75, 23);
            this.btnNewRegister.TabIndex = 14;
            this.btnNewRegister.Text = "Register";
            this.btnNewRegister.UseVisualStyleBackColor = true;
            this.btnNewRegister.Click += new System.EventHandler(this.btnNewRegister_Click);
            // 
            // btnSurvey
            // 
            this.btnSurvey.Location = new System.Drawing.Point(264, 136);
            this.btnSurvey.Name = "btnSurvey";
            this.btnSurvey.Size = new System.Drawing.Size(75, 23);
            this.btnSurvey.TabIndex = 17;
            this.btnSurvey.Text = "Survey";
            this.btnSurvey.UseVisualStyleBackColor = true;
            this.btnSurvey.Click += new System.EventHandler(this.btnSurvey_Click);
            // 
            // credit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(362, 306);
            this.Controls.Add(this.btnSurvey);
            this.Controls.Add(this.btnRequest);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.btnNewRegister);
            this.Controls.Add(this.lblFeedback);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.dtExp);
            this.Controls.Add(this.txtCvs);
            this.Controls.Add(this.txtCnumber);
            this.Controls.Add(this.txtCname);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "credit";
            this.Text = "Credit Card";
            this.Load += new System.EventHandler(this.credit_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtCname;
        private System.Windows.Forms.TextBox txtCnumber;
        private System.Windows.Forms.TextBox txtCvs;
        private System.Windows.Forms.DateTimePicker dtExp;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Label lblFeedback;
        private System.Windows.Forms.Button btnRequest;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnNewRegister;
        private System.Windows.Forms.Button btnSurvey;
    }
}