﻿namespace NewGym
{
    partial class classR
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblFeedback = new System.Windows.Forms.Label();
            this.txtRequestFN = new System.Windows.Forms.TextBox();
            this.txtRequestLN = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbPrivate = new System.Windows.Forms.ComboBox();
            this.cbInstructor = new System.Windows.Forms.ComboBox();
            this.cbClass = new System.Windows.Forms.ComboBox();
            this.btnRequest = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dtRequest = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.btnClass = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnNewRegister = new System.Windows.Forms.Button();
            this.btnSurvey = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblFeedback
            // 
            this.lblFeedback.AutoSize = true;
            this.lblFeedback.Location = new System.Drawing.Point(12, 220);
            this.lblFeedback.Name = "lblFeedback";
            this.lblFeedback.Size = new System.Drawing.Size(55, 13);
            this.lblFeedback.TabIndex = 0;
            this.lblFeedback.Text = "Feedback";
            // 
            // txtRequestFN
            // 
            this.txtRequestFN.Location = new System.Drawing.Point(113, 12);
            this.txtRequestFN.Name = "txtRequestFN";
            this.txtRequestFN.Size = new System.Drawing.Size(144, 20);
            this.txtRequestFN.TabIndex = 1;
            // 
            // txtRequestLN
            // 
            this.txtRequestLN.Location = new System.Drawing.Point(113, 38);
            this.txtRequestLN.Name = "txtRequestLN";
            this.txtRequestLN.Size = new System.Drawing.Size(144, 20);
            this.txtRequestLN.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(50, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "First Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(50, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Last Name";
            // 
            // cbPrivate
            // 
            this.cbPrivate.FormattingEnabled = true;
            this.cbPrivate.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cbPrivate.Location = new System.Drawing.Point(113, 65);
            this.cbPrivate.Name = "cbPrivate";
            this.cbPrivate.Size = new System.Drawing.Size(144, 21);
            this.cbPrivate.TabIndex = 5;
            // 
            // cbInstructor
            // 
            this.cbInstructor.FormattingEnabled = true;
            this.cbInstructor.Items.AddRange(new object[] {
            "John Petty",
            "Joe Shmoe",
            "Darth Vador",
            "Master Chief"});
            this.cbInstructor.Location = new System.Drawing.Point(113, 92);
            this.cbInstructor.Name = "cbInstructor";
            this.cbInstructor.Size = new System.Drawing.Size(144, 21);
            this.cbInstructor.TabIndex = 6;
            // 
            // cbClass
            // 
            this.cbClass.FormattingEnabled = true;
            this.cbClass.Items.AddRange(new object[] {
            "Mortal Kombat",
            "Light Saber",
            "Gun Control",
            "Dancing"});
            this.cbClass.Location = new System.Drawing.Point(113, 119);
            this.cbClass.Name = "cbClass";
            this.cbClass.Size = new System.Drawing.Size(144, 21);
            this.cbClass.TabIndex = 7;
            // 
            // btnRequest
            // 
            this.btnRequest.Location = new System.Drawing.Point(113, 191);
            this.btnRequest.Name = "btnRequest";
            this.btnRequest.Size = new System.Drawing.Size(75, 23);
            this.btnRequest.TabIndex = 8;
            this.btnRequest.Text = "Request";
            this.btnRequest.UseVisualStyleBackColor = true;
            this.btnRequest.Click += new System.EventHandler(this.btnRequest_Click_1);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(56, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Instructor";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(67, 68);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Private";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(76, 119);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Class";
            // 
            // dtRequest
            // 
            this.dtRequest.Location = new System.Drawing.Point(59, 146);
            this.dtRequest.Name = "dtRequest";
            this.dtRequest.Size = new System.Drawing.Size(198, 20);
            this.dtRequest.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 152);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Request";
            // 
            // btnClass
            // 
            this.btnClass.Location = new System.Drawing.Point(302, 73);
            this.btnClass.Name = "btnClass";
            this.btnClass.Size = new System.Drawing.Size(75, 23);
            this.btnClass.TabIndex = 16;
            this.btnClass.Text = "Class Request";
            this.btnClass.UseVisualStyleBackColor = true;
            this.btnClass.Click += new System.EventHandler(this.btnClass_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(302, 44);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 15;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnNewRegister
            // 
            this.btnNewRegister.Location = new System.Drawing.Point(302, 15);
            this.btnNewRegister.Name = "btnNewRegister";
            this.btnNewRegister.Size = new System.Drawing.Size(75, 23);
            this.btnNewRegister.TabIndex = 14;
            this.btnNewRegister.Text = "Register";
            this.btnNewRegister.UseVisualStyleBackColor = true;
            this.btnNewRegister.Click += new System.EventHandler(this.btnNewRegister_Click);
            // 
            // btnSurvey
            // 
            this.btnSurvey.Location = new System.Drawing.Point(302, 103);
            this.btnSurvey.Name = "btnSurvey";
            this.btnSurvey.Size = new System.Drawing.Size(75, 23);
            this.btnSurvey.TabIndex = 17;
            this.btnSurvey.Text = "Survey";
            this.btnSurvey.UseVisualStyleBackColor = true;
            this.btnSurvey.Click += new System.EventHandler(this.btnSurvey_Click);
            // 
            // classR
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(400, 339);
            this.Controls.Add(this.btnSurvey);
            this.Controls.Add(this.btnClass);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.btnNewRegister);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.dtRequest);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnRequest);
            this.Controls.Add(this.cbClass);
            this.Controls.Add(this.cbInstructor);
            this.Controls.Add(this.cbPrivate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtRequestLN);
            this.Controls.Add(this.txtRequestFN);
            this.Controls.Add(this.lblFeedback);
            this.Name = "classR";
            this.Text = "classR";
            this.Load += new System.EventHandler(this.classR_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblFeedback;
        private System.Windows.Forms.TextBox txtRequestFN;
        private System.Windows.Forms.TextBox txtRequestLN;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbPrivate;
        private System.Windows.Forms.ComboBox cbInstructor;
        private System.Windows.Forms.ComboBox cbClass;
        private System.Windows.Forms.Button btnRequest;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dtRequest;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnClass;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnNewRegister;
        private System.Windows.Forms.Button btnSurvey;
    }
}