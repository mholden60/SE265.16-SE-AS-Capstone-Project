﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;



namespace NewGym
{

    public partial class Form1 : Form
    {
        public Form1()
        {
            
            InitializeComponent();
 //login temp = new login();
 //           temp.ShowDialog();
            //if (lblPerson_id.Text > 1)
            //{ btnSubmit.Visible = false; }
            //else { btnSubmit.Visible = true; }
        }
        
        public void FillLabel(Person temp)
        {
            
            lblFeedback.Text = temp.FName + "\n";
            lblFeedback.Text = temp.LName + "\n";
            lblFeedback.Text = temp.Address + "\n";
            lblFeedback.Text = temp.Phone + "\n";
            lblFeedback.Text = temp.Birth + "\n";
            lblFeedback.Text = temp.Gender + "\n";


        }
       
        public Form1(Int32 intPerson_ID)
        {
           

            InitializeComponent();
            PersonV2 temp = new PersonV2();
            SqlDataReader dr = temp.FindOnePerson(intPerson_ID);

            while (dr.Read())
            {

                txtFname.Text = dr["FName"].ToString();
                txtLname.Text = dr["LName"].ToString();
                txtAddress.Text = dr["Address"].ToString();
                txtCity.Text = dr["City"].ToString();
                txtZip.Text = dr["Zip"].ToString();
                cbState.Text = dr["State"].ToString();
                dtDob.Text = dr["Birth"].ToString();
                cbGender.Text = dr["Gender"].ToString();
                txtPhone.Text = dr["Phone"].ToString();
               

                //store the ID in a new label
                lblPerson_id.Text = dr["Person_ID"].ToString();

            }


        }
        private void DisplayInfo(Person temp)
        {
            lblFeedback.Text = temp.FName + " " + temp.LName;
        }
        //private void DisplayInfo()
        //{
        //   lblFeedback.Text = "Unknown Person...Lack of Data";
        //}
        private void btnSubmit_Click(object sender, EventArgs e)
        {

            PersonV2 temp;
            temp = new PersonV2();
            temp.FName = txtFname.Text;
            temp.LName = txtLname.Text;
            temp.Phone = txtPhone.Text;
            temp.Address = txtAddress.Text;
            temp.Birth = dtDob.Text;
            temp.Gender = cbGender.Text;
            temp.City = txtCity.Text;
            temp.State = cbState.SelectedItem.ToString();
            temp.Zip = txtCity.Text;
            FillLabel(temp);

            if (temp.Feedback.Contains("ERROR: "))
            {
                lblFeedback.Text = temp.Feedback;
            }
            else
            {
                lblFeedback.Text = temp.AddContacts();
               // DisplayInfo();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'sE255_MHoldenDataSet.State' table. You can move, or remove it, as needed.
            this.stateTableAdapter.Fill(this.sE255_MHoldenDataSet.State);
            // TODO: This line of code loads data into the 'sE255_MHoldenDataSet.State' table. You can move, or remove it, as needed.
            if (txtFname.Text.Length > 0)
            {
                this.btnSubmit.Visible = false;
                btnSubmit.Enabled = false;
            }
            else
            {
                this.btnDelete.Visible = false;
                this.btnDelete.Enabled = false;
                this.btnUpdate.Visible = false;
                this.btnUpdate.Enabled = false;
            }

           
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            
            this.Hide();
            new search().Show();
        }

        private void btnRequest_Click(object sender, EventArgs e)
        {
            this.Hide();
            new classR().Show();
        }

        private void btnSurvey_Click(object sender, EventArgs e)
        {
            this.Hide();
            new survey().Show();
        }

        private void btnCredit_Click(object sender, EventArgs e)
        {
            this.Hide();
            new credit().Show();
        }

        private void cbGender_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        
        private void btnDelete_Click(object sender, EventArgs e)
        {
            //create a person so we can use the delete methode
            PersonV2 temp = new PersonV2();

            //convert the label text holding the ID to an integer
            Int32 intPerson_ID = Convert.ToInt32(lblPerson_id.Text);

            //Use the Person ID and pass it to the delete function and get the number of records deleted
            Int32 intRecords = temp.DeleteOnePerson(intPerson_ID);

            //Display feedback to the user
          lblFeedback.Text = intRecords.ToString() + " Records Deleted";
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (lblPerson_id.Text.Length < 0)
            {
           this.btnSubmit.Visible = false;
            }
            
            PersonV2 temp = new PersonV2();

            temp.FName = txtFname.Text;
            temp.LName = txtLname.Text;
            temp.Phone = txtPhone.Text;
            temp.Zip = txtZip.Text;
            temp.State = cbState.Text;
            temp.Address = txtAddress.Text;
            temp.Gender = cbGender.Text;
            temp.Birth = dtDob.Text;
            temp.City = txtCity.Text;
            
            
            temp.Person_ID = Convert.ToInt32(lblPerson_id.Text);

            
            if (temp.Feedback.Contains("ERROR: "))
            {
                lblFeedback.Text = temp.Feedback;
            }
            else if (temp.FName.Length > 0 && temp.LName.Length > 0)
            {
                DisplayInfo(temp);
                Int32 intRocords = temp.UpdateAContact();
                lblFeedback.Text = intRocords.ToString() + " Records Updated.";
            }
            else
            {
                DisplayInfo();
                Int32 intRocodrs = temp.UpdateAContact();
                lblFeedback.Text = intRocodrs.ToString() + " Records Updated.";
            }
        }
        
        private void DisplayInfo()
        {
            throw new NotImplementedException();
        }

        private void bindingSource1_CurrentChanged(object sender, EventArgs e)
        {

        }

        private void cbState_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbState.SelectedIndex == -1)
            {
                lblFeedback.Text = "Please choose Gender";
            }
        }

        private void fillByToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.stateTableAdapter.FillBy(this.sE255_MHoldenDataSet.State);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

        private void dtDob_ValueChanged(object sender, EventArgs e)
        {
           
        }

        private void txtZip_TextChanged(object sender, EventArgs e)
        {

        }

        private void dtDob_ValueChanged_1(object sender, EventArgs e)
        {
            
        }

        private void txtZip_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void txtFname_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
