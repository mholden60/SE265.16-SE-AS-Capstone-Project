﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Sql;
using System.Data;
using System.Data.SqlClient;

namespace NewGym
{
    class PersonV2 : Person
    {
        public const string connstring = @"Server=sql.neit.edu;Database=SE255_MHolden;User Id=SE255_MHolden;Password=001021282;";
        public PersonV2()
            : base()
        { }
        public Int32 Person_ID = 0;
        private string strFeedback;
        public string AddSurvey()
        {
            string strFeedback = "";
            string strSQL = "INSERT INTO Survey(Name, Stay, Needs, willReturn, Comment) VALUES (@Name, @Stay, @Needs, @willReturn, @Comment)";
            SqlConnection conn = new SqlConnection();
            string strConn = @connstring;
            conn.ConnectionString = strConn;

            SqlCommand comm = new SqlCommand();
            comm.CommandText = strSQL;
            comm.Connection = conn;

            comm.Parameters.AddWithValue("@Name", this.Name);
            comm.Parameters.AddWithValue("@Stay", this.Stay);
            comm.Parameters.AddWithValue("@Needs", this.Needs);
            comm.Parameters.AddWithValue("@willReturn", this.willReturn);
            comm.Parameters.AddWithValue("@Comment", this.Comment);


            try
            {
                conn.Open();
                strFeedback = comm.ExecuteNonQuery().ToString() + " Records Added";
                conn.Close();
            }
            catch (Exception err)
            {
                strFeedback = "ERROR: " + err.Message;
            }
            return strFeedback;

        
        }
        public string AddCard()
        {
            string strFeedback = "";
            string strSQL = "INSERT INTO Cards(CName, CNumber, CVS, Expire) VALUES (@CName, @CNumber, @CVS, @Expire)";
            SqlConnection conn = new SqlConnection();
            string strConn = @connstring;
            conn.ConnectionString = strConn;

            SqlCommand comm = new SqlCommand();
            comm.CommandText = strSQL;
            comm.Connection = conn;

            comm.Parameters.AddWithValue("@CName", this.CName );
            comm.Parameters.AddWithValue("@CNumber", this.CNumber);
            comm.Parameters.AddWithValue("@CVS", this.CVS);
            comm.Parameters.AddWithValue("@Expire", this.Expire);

            try
            {
                conn.Open();
                strFeedback = comm.ExecuteNonQuery().ToString() + " Records Added";
                conn.Close();
            }
            catch (Exception err)
            {
                strFeedback = "ERROR: " + err.Message;
            }
            return strFeedback;

        
        }
        public string AddContacts()
        {
            string strFeedback = "";

            string strSQL = "INSERT INTO Register (FName, LName, Address, City, State, Zip, Phone, Birth, Gender) VALUES (@FName, @LName, @Address, @City, @State, @Zip, @Phone, @Birth, @Gender)";
             SqlConnection conn = new SqlConnection();
             string strConn = @connstring;
            conn.ConnectionString = strConn;

            SqlCommand comm = new SqlCommand();
            comm.CommandText = strSQL;
            comm.Connection = conn;

            comm.Parameters.AddWithValue("@FName", FName);
            comm.Parameters.AddWithValue("@LName", LName);
            comm.Parameters.AddWithValue("@Address", Address);
            comm.Parameters.AddWithValue("@City", City);
            comm.Parameters.AddWithValue("@State", State);
            comm.Parameters.AddWithValue("@Zip", Zip);
            comm.Parameters.AddWithValue("@Phone", Phone);
            comm.Parameters.AddWithValue("@Birth", Birth);
            comm.Parameters.AddWithValue("@Gender", Gender);
            comm.Parameters.AddWithValue("@Person_ID", Person_ID);
            try
            {
                conn.Open();
                strFeedback = comm.ExecuteNonQuery().ToString() + " Records Added. \n";
                conn.Close();
            }
            catch (Exception err)
            {
                strFeedback = "ERROR: " + err.Message;
            }
            return strFeedback;

        
        }
        public string Survey()
        {
            
            string strFeedback = "";
            string strSQL = "INSERT INTO Survey(Name, Stay, Needs, willReturn, Comment) VALUES(@Name, @Stay, @Needs, @willReturn, @Comment)";
            SqlConnection conn = new SqlConnection();
            string strConn = @"Server=sql.neit.edu;Database=SE255_MHolden;User Id=SE255_MHolden;Password=001021282;";
            conn.ConnectionString = strConn;
            SqlCommand comm = new SqlCommand();
            comm.CommandText = strSQL;
            comm.Connection = conn;

          //  comm.Parameters.AddWithValue("@Fname", this.FName);// <----DO THIS FOR ALL OF THEM-------->>

            comm.Parameters.AddWithValue("@Name", this.Name);
            comm.Parameters.AddWithValue("@Stay", this.Stay);
            comm.Parameters.AddWithValue("@willReturn", this.willReturn);
            comm.Parameters.AddWithValue("@Needs", this.Needs);
            comm.Parameters.AddWithValue("@Comment", this.Comment);

            try
            {
                conn.Open();
                strFeedback = comm.ExecuteNonQuery().ToString() + " Records Added \n";
                strFeedback = "Thank you for your time";
                conn.Close();
            }
            catch (Exception err)
            {
                strFeedback = "ERROR: " + err.Message;
            }
            return strFeedback;


        }
        public string Request()
        {
            string strFeedback = "";
            string strSQL = "INSERT INTO Class (RequestFN, RequestLN,  Class, Instructor, dateRequest, Private) VALUES (@RequestFN, @RequestLN,  @Class, @Instructor, @dateRequest, @Private)";
            SqlConnection conn = new SqlConnection();
            string strConn = @"Server=sql.neit.edu;Database=SE255_MHolden;User Id=SE255_MHolden;Password=001021282;";
            conn.ConnectionString = strConn;
            SqlCommand comm = new SqlCommand();
            comm.CommandText = strSQL;
            comm.Connection = conn;

            comm.Parameters.AddWithValue("@RequestFN", this.RequestFN);
            comm.Parameters.AddWithValue("@RequestLN", this.RequestLN);
            comm.Parameters.AddWithValue("@Class", this.Class);
            comm.Parameters.AddWithValue("@Instructor", this.Instructor);
            comm.Parameters.AddWithValue("@dateRequest", this.dateRequest);
            comm.Parameters.AddWithValue("@Private", this.Private);

            try
            {
                conn.Open();
                strFeedback = comm.ExecuteNonQuery().ToString() + " Request Submitted";
                conn.Close();
            }
            catch (Exception err)
            {
                strFeedback = "ERROR: " + err.Message;
            }
            return strFeedback;

        }

        
        public DataSet SearchRecords(string txtFname, string txtLname,string txtAddress, string txtZip, string txtCity, string txtPhone, string cbGender)
        {

    
            DataSet ds = new DataSet();

            SqlConnection conn = new SqlConnection();
            string strConn = @"Server=sql.neit.edu;Database=SE255_MHolden;User Id=SE255_MHolden;Password=001021282;";
            conn.ConnectionString = strConn;

            SqlCommand comm = new SqlCommand();
            comm.Connection = conn;
            comm.CommandText = "SELECT * FROM Register WHERE 0=0";


            if (txtZip.Length > 0)
            {
                comm.CommandText += " AND Zip LIKE @Zip";
                comm.Parameters.AddWithValue("@Zip", txtZip);
            }

            if (txtCity.Length > 0)
            {
                comm.CommandText += " AND City LIKE @City";
                comm.Parameters.AddWithValue("@City", txtCity);
            }

            if (txtFname.Length > 0)
            {
                comm.CommandText += " AND FName LIKE @FName";
                comm.Parameters.AddWithValue("@FName", "%" + txtFname + "%");
            }

            if (txtLname.Length > 0)
            {
                comm.CommandText += " AND LName LIKE @LName";
                comm.Parameters.AddWithValue("@LName", "%" + txtLname + "%");
            }
         
            if (txtAddress.Length > 0)
            {
                comm.CommandText += " AND Address Like @Address";
                comm.Parameters.AddWithValue("@Address", txtAddress);
            }

            //if (dtDob.Length > 0)
            //{
            //    comm.CommandText += " AND Birth Like @Birth";
            //    comm.Parameters.AddWithValue("@Birth", dtDob);
            //}
            if (cbGender.Length > 0)
            {
                comm.CommandText += " AND Gender Like @Gender";
                comm.Parameters.AddWithValue("@Gender", cbGender);
            }
            //if (ddlState.Length > 0)
            //{
            //    comm.CommandText += " AND State Like @State";
            //    comm.Parameters.AddWithValue("@State", ddlState);
            //}
       
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = comm;
            conn.Open(); //open the connection
            //Perform search and fill ds
            da.Fill(ds, "CopyOfPersons");

            conn.Close(); //close the connection

            //Return the results
            return ds;
        }
           
        public SqlDataReader FindOnePerson(int intPerson_ID)
        {
            
            //Create and Innitialize the DB tools we need
            SqlConnection conn = new SqlConnection();
            SqlCommand comm = new SqlCommand();

            //My Connection String
            string strConn = @"Server=sql.neit.edu;Database=SE255_MHolden;User Id=SE255_MHolden;Password=001021282;";
             string sqlString = "SELECT Person_ID, LName, FName, Address, Phone, Birth, Gender, State, City, Zip FROM Register WHERE Person_ID = @Person_ID;";

            conn.ConnectionString = strConn;
            comm.Connection = conn;
            comm.CommandText = sqlString;
            comm.Parameters.AddWithValue("@Person_ID", intPerson_ID);

            conn.Open();

            return comm.ExecuteReader();

        }
            public Int32 DeleteOnePerson(int intPerson_ID)
            {
                 Int32 intRecords = 1;
              string strFeedback = "";
                //Create and Initialize the DB Tools we need
                SqlConnection conn = new SqlConnection();
                SqlCommand comm = new SqlCommand();

                //My Connection String
                string strConn = @connstring;

                //My SQL command string to pull up one person's data
                string sqlString =
               "DELETE FROM Register WHERE Person_ID = @Person_ID;";

                //Tell the connection object the who, what, where, how
                conn.ConnectionString = strConn;

                //Give the command object info it needs
                comm.Connection = conn;
                comm.CommandText = sqlString;
                comm.Parameters.AddWithValue("@Person_ID", intPerson_ID);

                //Open the DataBase Connection and Yell our SQL Command
                conn.Open();

                //Run the deletion and store the number of records effected
                strFeedback = comm.ExecuteNonQuery().ToString() + " Records Deleted";

                //close the connection
                conn.Close();

                return intRecords;   //Return # of records deleted

            }
        //******************************************************************************
        //*******************************START HERE*************************************
            public Int32 UpdateAContact()
            {
                
                Int32 intRecords = 0;
                
                string strSQL = "UPDATE Register SET FName = @FName, LNAME = @LName, Address = @Address, City = @City, State = @State, Zip = @Zip, Phone = @Phone, Birth = @Birth, Gender = @Gender WHERE Person_ID =  @Person_ID";

                SqlConnection conn = new SqlConnection();
                string strConn = @connstring;
                conn.ConnectionString = strConn;
                SqlCommand comm = new SqlCommand();
                comm.CommandText = strSQL;  
                comm.Connection = conn;

                comm.Parameters.AddWithValue("@FName", FName);
                comm.Parameters.AddWithValue("@LName", LName);
                comm.Parameters.AddWithValue("@Address", Address);
                comm.Parameters.AddWithValue("@City", City);
                comm.Parameters.AddWithValue("@State", State);
                comm.Parameters.AddWithValue("@Zip", Zip);
                comm.Parameters.AddWithValue("@Phone", Phone);
                comm.Parameters.AddWithValue("@Birth", Birth);
                comm.Parameters.AddWithValue("@Gender", Gender);
                comm.Parameters.AddWithValue("@Person_ID", Person_ID);

                try
                {
                    
                    conn.Open();

                    
                    intRecords = comm.ExecuteNonQuery();
                }
                catch (Exception err)
                {
                }
                finally
                {
                    
                    conn.Close();
                }

                return intRecords;




            }








        }
    }
