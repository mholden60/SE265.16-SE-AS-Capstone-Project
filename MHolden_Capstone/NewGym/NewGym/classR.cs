﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NewGym
{
    public partial class classR : Form
    {
        public classR()
        {
            InitializeComponent();
        }

        private void Class_Load(object sender, EventArgs e)
        {

        }

      
        private void btnRequest_Click_1(object sender, EventArgs e)
        {
            PersonV2 temp;
            temp = new PersonV2();
            temp.RequestFN = txtRequestFN.Text;
            temp.RequestLN = txtRequestLN.Text;
            temp.Private = cbPrivate.Text;
            temp.Instructor = cbInstructor.Text;
            temp.Class = cbClass.Text;
            temp.dateRequest = dtRequest.Text;

            if (temp.Feedback.Contains("ERROR: "))
            {
                lblFeedback.Text = temp.Feedback;
            }
            else
            {
                lblFeedback.Text = temp.Request();
                // DisplayInfo();
            }
        }

        private void classR_Load(object sender, EventArgs e)
        {

        }

        private void btnNewRegister_Click(object sender, EventArgs e)
        {
            this.Hide();
            new Form1().Show();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.Hide();
            new search().Show();

        }

        private void btnClass_Click(object sender, EventArgs e)
        {
            this.Hide();
            new classR().Show();
        }

        private void btnSurvey_Click(object sender, EventArgs e)
        {
            this.Hide();
            new survey().Show();
        }

    }
}
