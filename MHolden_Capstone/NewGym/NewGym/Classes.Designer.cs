﻿namespace NewGym
{
    partial class Class
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtRequestFN = new System.Windows.Forms.TextBox();
            this.txtRequestLN = new System.Windows.Forms.TextBox();
            this.cbClass = new System.Windows.Forms.ComboBox();
            this.cbInstructor = new System.Windows.Forms.ComboBox();
            this.dtRequest = new System.Windows.Forms.DateTimePicker();
            this.cbPrivate = new System.Windows.Forms.ComboBox();
            this.btnRequest = new System.Windows.Forms.Button();
            this.lblFeedback = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(86, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(192, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "Class Request";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "First Name:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(28, 191);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Private";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(28, 168);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Request Date";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(28, 138);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Instructor:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(28, 111);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Class:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(28, 85);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Last Name:";
            // 
            // txtRequestFN
            // 
            this.txtRequestFN.Location = new System.Drawing.Point(104, 56);
            this.txtRequestFN.Name = "txtRequestFN";
            this.txtRequestFN.Size = new System.Drawing.Size(121, 20);
            this.txtRequestFN.TabIndex = 7;
            // 
            // txtRequestLN
            // 
            this.txtRequestLN.Location = new System.Drawing.Point(104, 82);
            this.txtRequestLN.Name = "txtRequestLN";
            this.txtRequestLN.Size = new System.Drawing.Size(121, 20);
            this.txtRequestLN.TabIndex = 8;
            // 
            // cbClass
            // 
            this.cbClass.FormattingEnabled = true;
            this.cbClass.Location = new System.Drawing.Point(104, 108);
            this.cbClass.Name = "cbClass";
            this.cbClass.Size = new System.Drawing.Size(121, 21);
            this.cbClass.TabIndex = 9;
            // 
            // cbInstructor
            // 
            this.cbInstructor.FormattingEnabled = true;
            this.cbInstructor.Location = new System.Drawing.Point(104, 135);
            this.cbInstructor.Name = "cbInstructor";
            this.cbInstructor.Size = new System.Drawing.Size(121, 21);
            this.cbInstructor.TabIndex = 10;
            // 
            // dtRequest
            // 
            this.dtRequest.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtRequest.Location = new System.Drawing.Point(104, 162);
            this.dtRequest.Name = "dtRequest";
            this.dtRequest.Size = new System.Drawing.Size(121, 20);
            this.dtRequest.TabIndex = 11;
            // 
            // cbPrivate
            // 
            this.cbPrivate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPrivate.FormattingEnabled = true;
            this.cbPrivate.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cbPrivate.Location = new System.Drawing.Point(104, 188);
            this.cbPrivate.Name = "cbPrivate";
            this.cbPrivate.Size = new System.Drawing.Size(121, 21);
            this.cbPrivate.TabIndex = 12;
            // 
            // btnRequest
            // 
            this.btnRequest.Location = new System.Drawing.Point(104, 227);
            this.btnRequest.Name = "btnRequest";
            this.btnRequest.Size = new System.Drawing.Size(75, 23);
            this.btnRequest.TabIndex = 13;
            this.btnRequest.Text = "Request";
            this.btnRequest.UseVisualStyleBackColor = true;
            this.btnRequest.Click += new System.EventHandler(this.btnRequest_Click);
            // 
            // lblFeedback
            // 
            this.lblFeedback.AutoSize = true;
            this.lblFeedback.Location = new System.Drawing.Point(31, 262);
            this.lblFeedback.Name = "lblFeedback";
            this.lblFeedback.Size = new System.Drawing.Size(55, 13);
            this.lblFeedback.TabIndex = 14;
            this.lblFeedback.Text = "Feedback";
            // 
            // Class
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(374, 394);
            this.Controls.Add(this.lblFeedback);
            this.Controls.Add(this.btnRequest);
            this.Controls.Add(this.cbPrivate);
            this.Controls.Add(this.dtRequest);
            this.Controls.Add(this.cbInstructor);
            this.Controls.Add(this.cbClass);
            this.Controls.Add(this.txtRequestLN);
            this.Controls.Add(this.txtRequestFN);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Class";
            this.Text = "Class Request";
            this.Load += new System.EventHandler(this.Class_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtRequestFN;
        private System.Windows.Forms.TextBox txtRequestLN;
        private System.Windows.Forms.ComboBox cbClass;
        private System.Windows.Forms.ComboBox cbInstructor;
        private System.Windows.Forms.DateTimePicker dtRequest;
        private System.Windows.Forms.ComboBox cbPrivate;
        private System.Windows.Forms.Button btnRequest;
        private System.Windows.Forms.Label lblFeedback;
    }
}