﻿namespace NewGym
{
    partial class survey
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtComment = new System.Windows.Forms.TextBox();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.lblFeedback = new System.Windows.Forms.Label();
            this.btnNewRegister = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnRequest = new System.Windows.Forms.Button();
            this.btnCredit = new System.Windows.Forms.Button();
            this.cbStay = new System.Windows.Forms.ComboBox();
            this.cbNeed = new System.Windows.Forms.ComboBox();
            this.cbReturn = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(90, 33);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(204, 20);
            this.txtName.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "How was your stay?";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(28, 130);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(109, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Gym meet your needs";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(31, 199);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(113, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Chances on Returning";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(31, 313);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Comment:";
            // 
            // txtComment
            // 
            this.txtComment.Location = new System.Drawing.Point(31, 330);
            this.txtComment.Multiline = true;
            this.txtComment.Name = "txtComment";
            this.txtComment.Size = new System.Drawing.Size(389, 71);
            this.txtComment.TabIndex = 5;
            // 
            // btnSubmit
            // 
            this.btnSubmit.Location = new System.Drawing.Point(69, 407);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(75, 23);
            this.btnSubmit.TabIndex = 9;
            this.btnSubmit.Text = "Submit";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // lblFeedback
            // 
            this.lblFeedback.AutoSize = true;
            this.lblFeedback.Location = new System.Drawing.Point(31, 275);
            this.lblFeedback.Name = "lblFeedback";
            this.lblFeedback.Size = new System.Drawing.Size(55, 13);
            this.lblFeedback.TabIndex = 10;
            this.lblFeedback.Text = "Feedback";
            // 
            // btnNewRegister
            // 
            this.btnNewRegister.Location = new System.Drawing.Point(454, 95);
            this.btnNewRegister.Name = "btnNewRegister";
            this.btnNewRegister.Size = new System.Drawing.Size(75, 23);
            this.btnNewRegister.TabIndex = 11;
            this.btnNewRegister.Text = "Register";
            this.btnNewRegister.UseVisualStyleBackColor = true;
            this.btnNewRegister.Click += new System.EventHandler(this.btnNewRegister_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(454, 124);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 12;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnRequest
            // 
            this.btnRequest.Location = new System.Drawing.Point(454, 153);
            this.btnRequest.Name = "btnRequest";
            this.btnRequest.Size = new System.Drawing.Size(75, 23);
            this.btnRequest.TabIndex = 13;
            this.btnRequest.Text = "Class Request";
            this.btnRequest.UseVisualStyleBackColor = true;
            this.btnRequest.Click += new System.EventHandler(this.btnRequest_Click);
            // 
            // btnCredit
            // 
            this.btnCredit.Location = new System.Drawing.Point(454, 182);
            this.btnCredit.Name = "btnCredit";
            this.btnCredit.Size = new System.Drawing.Size(75, 23);
            this.btnCredit.TabIndex = 18;
            this.btnCredit.Text = "Credit Card";
            this.btnCredit.UseVisualStyleBackColor = true;
            this.btnCredit.Click += new System.EventHandler(this.btnCredit_Click);
            // 
            // cbStay
            // 
            this.cbStay.FormattingEnabled = true;
            this.cbStay.Items.AddRange(new object[] {
            "Poor",
            "Fair",
            "Great"});
            this.cbStay.Location = new System.Drawing.Point(34, 83);
            this.cbStay.Name = "cbStay";
            this.cbStay.Size = new System.Drawing.Size(121, 21);
            this.cbStay.TabIndex = 2;
            this.cbStay.SelectedIndexChanged += new System.EventHandler(this.cbStay_SelectedIndexChanged);
            // 
            // cbNeed
            // 
            this.cbNeed.FormattingEnabled = true;
            this.cbNeed.Items.AddRange(new object[] {
            "Poor",
            "Fair",
            "Great"});
            this.cbNeed.Location = new System.Drawing.Point(31, 153);
            this.cbNeed.Name = "cbNeed";
            this.cbNeed.Size = new System.Drawing.Size(121, 21);
            this.cbNeed.TabIndex = 3;
            this.cbNeed.SelectedIndexChanged += new System.EventHandler(this.cbNeed_SelectedIndexChanged);
            // 
            // cbReturn
            // 
            this.cbReturn.FormattingEnabled = true;
            this.cbReturn.Items.AddRange(new object[] {
            "Poor",
            "Fair",
            "Great"});
            this.cbReturn.Location = new System.Drawing.Point(31, 225);
            this.cbReturn.Name = "cbReturn";
            this.cbReturn.Size = new System.Drawing.Size(121, 21);
            this.cbReturn.TabIndex = 4;
            this.cbReturn.SelectedIndexChanged += new System.EventHandler(this.cbReturn_SelectedIndexChanged);
            // 
            // survey
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(541, 442);
            this.Controls.Add(this.cbReturn);
            this.Controls.Add(this.cbNeed);
            this.Controls.Add(this.cbStay);
            this.Controls.Add(this.btnCredit);
            this.Controls.Add(this.btnRequest);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.btnNewRegister);
            this.Controls.Add(this.lblFeedback);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.txtComment);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.label1);
            this.Name = "survey";
            this.Text = "survey";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtComment;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Label lblFeedback;
        private System.Windows.Forms.Button btnNewRegister;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnRequest;
        private System.Windows.Forms.Button btnCredit;
        private System.Windows.Forms.ComboBox cbStay;
        private System.Windows.Forms.ComboBox cbNeed;
        private System.Windows.Forms.ComboBox cbReturn;

    }
}