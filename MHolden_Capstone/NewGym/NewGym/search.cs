﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NewGym
{
    public partial class search : Form
    {
        public search()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            new Form1().Show();
        }

        private void btnRequest_Click(object sender, EventArgs e)
        {
            new classR().Show();
        }

        private void btnNewRegister_Click(object sender, EventArgs e)
        {
            this.Hide();
            new Form1().Show();
        }

        private void btnSurvey_Click(object sender, EventArgs e)
        {
            this.Hide();
            new survey().Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            new classR().Show();
        }

        private void btnCredit_Click(object sender, EventArgs e)
        {
            this.Hide();
            new credit().Show();
        }

        private void search_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'sE255_MHoldenDataSet.State' table. You can move, or remove it, as needed.
            this.stateTableAdapter.Fill(this.sE255_MHoldenDataSet.State);
           
        }

        private void gvResults_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string strPerson_ID = gvResults.Rows[e.RowIndex].Cells[0].Value.ToString();
            //MessageBox.Show(strPerson_ID);
            int intPerson_ID = Convert.ToInt32(strPerson_ID);

            Form1 Editor = new Form1(intPerson_ID);
            Editor.ShowDialog();
            
            
        }

        private void btnSearch_Click_1(object sender, EventArgs e)
        {

        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
                        //Call the search function in Person_v2
            PersonV2 temp = new PersonV2();
            DataSet ds = temp.SearchRecords(txtFname.Text, txtLname.Text, txtAddress.Text, txtZip.Text, txtCity.Text, txtPhone.Text, cbGender.Text);
            
            //Get the results and display
            gvResults.DataSource = ds;
            gvResults.DataMember = "CopyOfPersons";
            //gvResults.
        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void fillBy1ToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.stateTableAdapter.FillBy1(this.sE255_MHoldenDataSet.State);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

        private void lblFeedback5_Click(object sender, EventArgs e)
        {

        }


        

    }
}
